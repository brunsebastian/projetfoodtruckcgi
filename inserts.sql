INSERT INTO `springmvc`.`user`
(`id`,
`genre`,
`nom`,
`prenom`,
`pwd`,
`soc`,
`version`,
`id_profil`)
VALUES
(1,
'femme',
'Drari',
'Yasmine',
'pwd1',
'CGI',
0,
1);
INSERT INTO `springmvc`.`profil`
(`id`,
`profil`,
`version`)
VALUES
(1,
'Douceur',
0);
INSERT INTO `springmvc`.`produit`
(`id_produit`,
`compo`,
`description`,
`e_famille`,
`imgurl`,
`jour_dispo`,
`nom`,
`prix`,
`stock`,
`version`)
VALUES
(1,
'Attention plein de malbouffe',
'Mais ça bon!',
'dessert',
'https://cdn.pixabay.com/photo/2017/01/11/11/33/cake-1971552_960_720.jpg',
'1110011',
'Dessert du Chef',
10,
20,
0);
INSERT INTO `springmvc`.`produit`
(`id_produit`,
`compo`,
`description`,
`e_famille`,
`imgurl`,
`jour_dispo`,
`nom`,
`prix`,
`stock`,
`version`)
VALUES
(2,
'Attention plein de malbouffe',
'Mais ça bon!',
'dessert',
'https://cdn.pixabay.com/photo/2017/05/07/08/56/pancakes-2291908_960_720.jpg',
'1000011',
'Dessert surprise',
12,
30,
0);
INSERT INTO `springmvc`.`produit`
(`id_produit`,
`compo`,
`description`,
`e_famille`,
`imgurl`,
`jour_dispo`,
`nom`,
`prix`,
`stock`,
`version`)
VALUES
(3,
'Attention plein de malbouffe',
'Mais ça bon!',
'dessert',
'https://cdn.pixabay.com/photo/2016/10/31/18/14/ice-1786311_960_720.jpg',
'1111111',
'Dessert du Jour',
6,
50,
0);
INSERT INTO `springmvc`.`produit`
(`id_produit`,
`compo`,
`description`,
`e_famille`,
`imgurl`,
`jour_dispo`,
`nom`,
`prix`,
`stock`,
`version`)
VALUES
(4,
'Attention plein de malbouffe',
'Mais ça bon!',
'dessert',
'https://cdn.pixabay.com/photo/2016/11/22/18/52/cake-1850011_960_720.jpg',
'0101011',
'Pudding',
5,
20,
0);
INSERT INTO `springmvc`.`actu`
(`id`,
`date_create`,
`date_valid`,
`description`,
`image_url`,
`title`,
`version`,
`user_create`)
VALUES
(1,
'2020-03-05',
'2020-03-30',
'desc1',
'https://cdn.pixabay.com/photo/2019/04/27/22/09/space-4161418_960_720.jpg',
'tit1',
0,
1);
INSERT INTO `springmvc`.`actu`
(`id`,
`date_create`,
`date_valid`,
`description`,
`image_url`,
`title`,
`version`,
`user_create`)
VALUES
(2,
'2020-03-05',
'2020-04-30',
'desc2',
'https://cdn.pixabay.com/photo/2020/02/28/19/57/sunset-4888423_960_720.jpg',
'tit1',
0,
1);
INSERT INTO `springmvc`.`prod_type`
(`id`,
`h_limit`,
`type_repas`,
`version`)
VALUES
(1,
11,
'Petit-Déjeuner',
0);
INSERT INTO `springmvc`.`avis`
(`id`,
`version`,
`comm`,
`note`,
`id_prod`,
`id_user`)
VALUES
(1,
0,
'Excellent',
5,
1,
1);
INSERT INTO `springmvc`.`adresse_facturation`
(`id`,
`cp`,
`is_active`,
`num`,
`pays`,
`rue`,
`version`,
`ville`,
`id_user`)
VALUES
(1,
'75013',
TRUE,
134,
'FRANCE',
'Rue de Tolbiac',
0,
'Paris',
1);
INSERT INTO `springmvc`.`adresse_livraison`
(`id`,
`cp`,
`is_active`,
`num`,
`pays`,
`rue`,
`version`,
`ville`,
`id_user`)
VALUES
(1,
'75013',
TRUE,
134,
'FRANCE',
'Rue de Tolbiac',
0,
'Paris',
1);
INSERT INTO `springmvc`.`prod_type_prods`
(`prod_type_id`,
`prods_id_produit`)
VALUES
(1,
1);
INSERT INTO `springmvc`.`disp_type_prod`
(`id_produit`,
`prod_type`)
VALUES
(1,
1);
INSERT INTO `springmvc`.`commande`
(`id`,
`date_cmd`,
`date_livr`,
`type_cmd`,
`version`,
`adr_factu`,
`adr_livr`,
`id_user`)
VALUES
(1,
'2020-03-05',
'2020-03-05',
'emporter',
0,
1,
1,
1);
INSERT INTO `springmvc`.`commande`
(`id`,
`date_cmd`,
`date_livr`,
`type_cmd`,
`version`,
`adr_factu`,
`adr_livr`,
`id_user`)
VALUES
(2,
'2020-03-05',
'2020-03-06',
'emporter',
0,
1,
1,
1);
INSERT INTO `springmvc`.`ligne_commande`
(`id`,
`prix_unit`,
`qte`,
`version`,
`id_cmd`,
`id_prod`)
VALUES
(1,
10,
2,
0,
1,
1);
INSERT INTO `springmvc`.`ligne_commande`
(`id`,
`prix_unit`,
`qte`,
`version`,
`id_cmd`,
`id_prod`)
VALUES
(2,
12,
1,
0,
1,
2);
INSERT INTO `springmvc`.`ligne_commande`
(`id`,
`prix_unit`,
`qte`,
`version`,
`id_cmd`,
`id_prod`)
VALUES
(3,
6,
5,
0,
1,
3);
INSERT INTO `springmvc`.`ligne_commande`
(`id`,
`prix_unit`,
`qte`,
`version`,
`id_cmd`,
`id_prod`)
VALUES
(4,
10,
10,
0,
2,
1);
INSERT INTO `springmvc`.`ligne_commande`
(`id`,
`prix_unit`,
`qte`,
`version`,
`id_cmd`,
`id_prod`)
VALUES
(5,
5,
1,
0,
2,
4);
INSERT INTO `springmvc`.`ligne_commande`
(`id`,
`prix_unit`,
`qte`,
`version`,
`id_cmd`,
`id_prod`)
VALUES
(6,
5,
1,
0,
1,
4);