export class Product {
    idProduit: number;
    nom: string;
    prix: number;
    description: string;
    imgURL: string;
    jourDispo: string;
    stock: number;
    compo: string;
    eFamille: string;
    version: number;
}
