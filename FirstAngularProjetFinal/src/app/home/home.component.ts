import { Component, OnInit } from '@angular/core';
import { Actu } from '../models/actu';
import { AccueilService } from '../services/accueil.service';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  actualite : Array<Actu>= new Array<Actu>();
  top3 : Array<Product>= new Array<Product>();

  constructor(private _products : AccueilService , private _top3 : ProductService) { }

  ngOnInit(): void {
    this.getActu();
  }

  getActu(){
    this._products.GetActu().subscribe((actualites: Actu[]) => {
      console.log(actualites);
        this.actualite = actualites;
    });     
  }

  getTop3Prod(){
    this._top3.GetProductTop3().subscribe((top3: Product[]) => {
        this.top3 = top3;
    });     
  }

}
