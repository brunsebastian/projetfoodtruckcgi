import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actu } from '../models/actu';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AccueilService {

  private _actuUrl = 'http://localhost:8082/actus/all';

  constructor(private _http:HttpClient ) { }

  GetActu() : Observable<Actu[]>{
    return this._http.get<Actu[]>(this._actuUrl);
 }
}
