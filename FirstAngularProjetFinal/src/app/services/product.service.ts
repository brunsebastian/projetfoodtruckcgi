import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private _productUrl = "./assets/api/products/products.json";
  private _producttop3Url = "http://localhost:8082/actus/top3";

  constructor(private _http:HttpClient) { }

  GetProducts(){
    return this._http.get(this._productUrl);
  }

  GetProductTop3(){
    return this._http.get(this._producttop3Url);
  }
}
